import { objectAny } from "uni-simple-router"

export const Utils = {
	formatTime: (time : any) => {
		if (typeof time !== 'number' || time < 0) {
			return time
		}

		var hour = parseInt(time / 3600 + "")
		time = time % 3600
		var minute = parseInt(time / 60 + "")
		time = time % 60
		var second = time

		return ([hour, minute, second]).map(function (n) {
			n = n.toString()
			return n[1] ? n : '0' + n
		}).join(':')
	},
	toTimestamp: (num : any) => {
		var n = 10;
		if (num || num < 14) {
			n = num;
		}
		return parseInt((Date.now() + '').substring(0, n));
	},
	/**var time=new Date(parseInt(1420184730) * 1000).format('yyyy年M月d日');
	 * 月(M)、日(d)、小时(h)、分(m)、秒(s) 毫秒(S)、季度(q) 可以用 1-2 个占位符，
	 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
	 * 例子：
	 * (new Date()).format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
	 * */
	formatDate: (timep : any, timetype : any) => {
		if (timep && parseInt(timep) != timep) {
			// 判断时间戳是否是数字，不是数字原数据返回
			return timep;
		} else if (!timep) {
			timep = parseInt(new Date().valueOf() / 1000 + "");
		} else if (timep.length == 10) {
			timep = parseInt(timep)
		} else if (timep.length > 10) {
			timep = timep.substring(0, 10);
		}
		// return '收到'+timep;

		var timetype = timetype || "YYYY-mm-dd HH:MM:SS";

		//  Date.prototype.format = function(params: any) {
		// this.apply(this, params);
		//  };
		// var format = function (date:any, fmt:any) { //author: meizz
		// 	var o = {
		// 		"M+": date.getMonth() + 1, //月份
		// 		"d+": date.getDate(), //日
		// 		"h+": date.getHours(), //小时
		// 		"m+": date.getMinutes(), //分
		// 		"s+": date.getSeconds(), //秒
		// 		"q+": Math.floor((date.getMonth() + 3) / 3), //季度
		// 		"S": date.getMilliseconds() //毫秒
		// 	};
		// 	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substring(4 - RegExp.$1.length));
		// 	for (var k in o)
		// 		if (new RegExp("(" + k + ")").test(fmt))
		// 			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substring(("" + o[k]).length)));
		// 	return fmt;
		// }
		// var time = format(new Date(parseInt(timep) * 1000), timetype);
		var time = dateFormatL(timetype, new Date(parseInt(timep) * 1000))
		return time;
	}

	/**
	 * 获取时间戳 默认10位,
	 */
	,
	getTime: (num : number) => {
		// var num = parseInt(num+"");
		if (isNaN(num)) {
			num = 1000;
		}

		return parseInt(new Date().valueOf() + "") / num;
	}, // "YYYY-mm-dd HH:MM"
	dateFormatL(fmt = "YYYY-mm-dd HH:MM:SS", date : Date) {
		if (date == null) {
			date = new Date()
		}
		let ret : RegExpExecArray | null;
		var opt:objectAny
		opt= {
			'Y+': date.getFullYear().toString(), // 年
			'm+': (date.getMonth() + 1).toString(), // 月
			'd+': date.getDate().toString(), // 日
			'H+': date.getHours().toString(), // 时
			'M+': date.getMinutes().toString(), // 分
			'S+': date.getSeconds().toString(), // 秒
			// 有其他格式化字符需求可以继续添加，必须转化成字符串
		}
		for (let k in opt) {
			ret = new RegExp('(' + k + ')').exec(fmt)
			if (ret) {
				var str : string
				str = ret[1].length == 1 ? opt[k] : opt[k].padStart(ret[1].length, '0')
				fmt = fmt.replace(ret[1], str)
			}
		}
		return fmt
	}
}
//距离时间
export var dateUtils : object
dateUtils = {
	UNITS: {
		'年': 31557600000,
		'月': 2629800000,
		'天': 86400000,
		'小时': 3600000,
		'分钟': 60000,
		'秒': 1000
	},
	humanize: (milliseconds : any) => {
		var humanize = '';
		var that : any
		that = this
		for (var key in that.UNITS) {
			if (milliseconds >= that.UNITS[key]) {
				humanize = Math.floor(milliseconds / that.UNITS[key]) + key + '前';
				break;
			}
		}
		return humanize || '刚刚';
	},
	format: (dateStr : any) => {
		var that : any
		that = this
		var date = that.parse(dateStr)
		var diff = Date.now() - date.getTime();
		if (diff < that.UNITS['天']) {
			return that.humanize(diff);
		}
		var _format = (number : any) => {
			return (number < 10 ? ('0' + number) : number);
		};
		return date.getFullYear() + '/' + _format(date.getMonth() + 1) + '/' + _format(date.getDay()) + '-' +
			_format(date.getHours()) + ':' + _format(date.getMinutes());
	},
	parse: (str : any) => { //将"yyyy-mm-dd HH:MM:ss"格式的字符串，转化为一个Date对象
		var a = str.split(/[^0-9]/);
		return new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
	}
}


// 距离时间
export function dataFormat(time : any, option : any) {
	time = +time * 1000
	var d : any
	d = new Date(time)
	const now = new Date().getTime()

	const diff = (now - d) / 1000
	if (diff < 30) {
		return '刚刚'
	} else if (diff < 3600) {
		// less 1 hour
		return Math.ceil(diff / 60) + '分钟前'
	} else if (diff < 3600 * 24) {
		return Math.ceil(diff / 3600) + '小时前'
	} else if (diff < 3600 * 24 * 2) {
		return '1天前'
	}
	if (option) {
		return Utils.dateFormatL(option, d);
	} else {
		let timeStr = d.getFullYear() + '年' + (d.getMonth() + 1) + '月' + d.getDate() + '日' + d.getHours() + '时' + d
			.getMinutes() + '分'
		return timeStr
	}
}
// 兩個时间相差秒
export function diffTime(time : string | number | Date, time2 = null) {
	var d = new Date(time).valueOf();
	// var d:number = new Date(time1)
	var now : number;
	if (time2 == null) {
		now = new Date().getTime()
	} else {
		now = new Date(time2).valueOf();
	}
	// console.log([time,time1,d.getTime(),now])

	const diff : number = (now - d) / 1000
	// console.log(diff)
	return {
		s: parseInt(diff + ""),
		m: parseInt(diff / 60 + ""),
		h: parseInt(diff / 3600 + ""),
		d: parseInt(diff / (3600 * 24) + "")
	}
}
export var toTimestamp = Utils.toTimestamp; //日期时间转成时间戳
export var getTime = Utils.getTime; //当前时间戳
export var formatDate = Utils.formatDate; //时间戳转日期
export var dateFormatL = Utils.dateFormatL;//获取new Date 返回日期时间格式  "YYYY-mm-dd HH:MM"
export default {
	diffTime, dataFormat, dateFormatL, toTimestamp, getTime, formatDate, dateUtils, Utils
}