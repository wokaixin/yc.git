export function getBrowser() {
	var ret : object
	ret = {
		// name: "MiniWeixin",
		name: "Other",
		version: "1.0.0"
	};
	// #ifdef H5
	let explorer : string
	explorer = window.navigator.userAgent.toLowerCase() || "";
	if (explorer.indexOf("msie") >= 0) {
		let verarr : RegExpMatchArray | null = explorer.match(/msie ([\d.]+)/);
		var ver : string = verarr != null ? verarr[1] + "" : "";
		return {
			name: "IE",
			version: ver
		};
	} else if (explorer.indexOf("firefox") >= 0) {
		let verarr : RegExpMatchArray | null = explorer.match(/firefox\/([\d.]+)/)
		var ver : string = verarr != null ? verarr[1] + "" : "";
		return {
			name: "Firefox",
			version: ver
		};
	} else if (explorer.indexOf("chrome") >= 0) {
		let verarr : RegExpMatchArray | null = explorer.match(/chrome\/([\d.]+)/);
		var ver : string = verarr != null ? verarr[1] + "" : "";
		return {
			name: "Chrome",
			version: ver
		};
	} else if (explorer.indexOf("opera") >= 0) {
		let verarr : RegExpMatchArray | null = explorer.match(/opera.([\d.]+)/);
		var ver : string = verarr != null ? verarr[1] + "" : "";
		return {
			name: "Opera",
			version: ver
		};
	} else if (explorer.indexOf("Safari") >= 0) {
		let verarr : RegExpMatchArray | null = explorer.match(/version\/([\d.]+)/);
		var ver : string = verarr != null ? verarr[1] + "" : "";
		return {
			name: "Safari",
			version: ver
		};
	}
	// #endif
	return ret;
}
export default { getBrowser }