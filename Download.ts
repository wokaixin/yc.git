// 下载文件
export function downloadFile(obj : any, name : any, suffix : any) {
	const url = window.URL.createObjectURL(new Blob([obj]))
	const link = document.createElement('a')
	link.style.display = 'none'
	link.href = url
	const fileName = parseTime(new Date()) + '-' + name + '.' + suffix
	link.setAttribute('download', fileName)
	document.body.appendChild(link)
	link.click()
	document.body.removeChild(link)
}

export default { downloadFile }

function parseTime(date : Date) {
	date = date || new Date;
	var year:any = date.getFullYear()
	var month:any = date.getMonth() + 1
	var day:any = date.getDate();
	// 因为当年月日中间是 ‘-’ 短横线的时候，它的解析是用UTC 时区处理，而不是用本地时区处理的，因此相差八个小时，就成了这个时间点到1970年1月1日08:00的毫秒数。
	// 解决办法就是将日期中的短横线替换成 ‘/’ ,
	// getTime()和Date.parse()方法 都会有相同的情况
	return Date.parse(year + '-' + month + '-' + day);
}