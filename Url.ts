import { objectAny } from "uni-simple-router";

// console.log(urlEncode(arr||obj).slice(1))//调用
export function ArrToUrl(param:any) {
	var paramStr = '';
	if (param instanceof Array) {
		for (let i = 0; i < param.length; i++) {
			for (var p in param[i]) {
				paramStr += p + '=' + param[i][p] + '&';
			}
		}
	} else if (param instanceof Object) {
		for (var p in param) {
			paramStr += p + '=' + param[p] + '&';
		}
	}
	paramStr = paramStr.substring(0, paramStr.lastIndexOf('?'));
	return paramStr;

}
export function encodeObj(obj:any) {
	return encodeURIComponent(JSON.stringify(obj))
}
export function decodeObj(obj:any) {
	return JSON.parse(decodeURIComponent(obj))
}
export function ObjToUrl(json:any,noKeys:Array<string>= []) {
	var str = Object.keys(json).map(function(key) {
		if(noKeys.indexOf(key)==-1){
			return encodeURIComponent(key) + "=" + encodeURIComponent(json[key]);
		}
		// return encodeURIComponent(key) + "=" + encodeURIComponent(json[key]);
	}).join("&");
	if (str.substr(0, 1) == "&") {
		// 删除第一个字符 
		str = str.slice(1);
	}
	return str;
}
export function UrlToObj(str:any) {
	var obj:objectAny;
	obj={}
	var arr1:Array<string> = str.split("?");
	var arr2:Array<string> = arr1[1].split("&");
	for (var i = 0; i < arr2.length; i++) {
		var res:Array<string> = arr2[i].split("=");
		obj[res[0]] = res[1];
	}
	return obj;
}
export default {
	// CusBASE64: __BASE64,
	ArrToUrl: ArrToUrl,
	ObjToUrl: ObjToUrl, //
	UrlToObj: UrlToObj,
	decodeObj,
	encodeObj
	// encoder:base64decode
}
