js工具插件
#介绍
js请选版本1
ts请选版本2

#功能接口 
>1，Time时间戳转换处理 
2，Arr 数组处理 
3，Base64 base64解码加密处理 
4，File 文件处理 
5，Id 唯一id生产 
6，Md5 MD5生成工具 
7，Storage 缓存处理 
8，Url url地址处理 
9，Validate 表单验证
10 area 全国地区表 为了压缩空间 目前只有省级
11 Str 一些字符串处理
12 Browser 浏览器相关


#安装
```
npm install git+https://gitee.com/wokaixin/yc.git
```
#使用说明
vue 全部中引入
```
import yc from 'yc';
```
或者按需引入
```
import {Time,Arr,Base64,File,Id,Md5,Storage,Url,Validate,area} from 'yc';
```
#使用
yc.Arr
yc.Time
...
 
具体都有哪些接口 可以打印yc查看

参与贡献
作者 yichen
交流uni-app+vue qq群714566447