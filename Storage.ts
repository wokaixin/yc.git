// // console.log(urlEncode(arr||obj).slice(1))//调用
var cacheExpires =  172800;//默认缓存周期秒=48小时
var maxExpires=31104000;//最长缓存时间1年
// 判断是数字
function checkRate(nubmer:any) {
　　var re = /^[0-9]+.?[0-9]*$/; //判断字符串是否为数字 //判断正整数 /^[1-9]+[0-9]*]*$/ 
　　return re.test(nubmer);
}
//计算合法缓存时间
function computeExpires(expires:any){
        var expires = expires || cacheExpires;
        var nowTime=Date.parse(new Date()+"") / 1000;
        if(checkRate(expires) ){
            if(expires>nowTime){
               return expires
            }else if(expires<maxExpires){
                return expires+nowTime
            }
        }
           return cacheExpires+nowTime
}
export var Sync = {
    /**
     * 异步存入缓存 可对象可数组
     * k 		string 				键
     * val 		array|object|string	缓存的内容
     * expires	int					有效期
     */
    set(k:string, val:any, expires:Number) {
        var type = typeof val;
		// localStorage.setItem
        return uni.setStorageSync(k, JSON.stringify({
            data: val,
            expires: computeExpires(expires),
            type: type
        }));
    },
    get(k:string) {
        try {
			// localStorage.getItem
            var data = uni.getStorageSync(k) || '{}';
            data = JSON.parse(data)
            if (data.expires) {
                if (data.expires > (Date.parse(new Date()+"") / 1000)) {
                    return data.data;
                }
				// localStorage.removeItem
				
                uni.removeStorageSync(k);
                try {
                    uni.removeStorageSync(k);
                } catch (e) {
                    // error
                }
            }
        } catch (e) {
            return false;
            //TODO handle the exception
        }

        return false;

    },
    remove(k:string) {
        uni.removeStorageSync(k);
    },
    clear() {
		
        // localStorage.clear();
		uni.clearStorageSync()
    }
}

export function clear() {
    // sessionStorage.clear();
    // localStorage.clear();
	uni.clearStorageSync()
}

export default {
    Sync,
    clear
}
