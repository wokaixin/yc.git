
//提取字符串里的中文
export function toZhongwen(str:string){
	var patt1 = /[\u4e00-\u9fa5]+/g;
	if(str==null || patt1==null){
		return ""
	}
	//利用join将结果转换成字符串
	var  letters:RegExpMatchArray | null = str.match(patt1)
	if(letters==null){
		return ""
	}
	return letters.join("");
}
//把utf16的emoji表情字符进行转码成八进制的字符
export function utf16toEntities(str:any) {
	var patt = /[\ud800-\udbff][\udc00-\udfff]/g; // 检测utf16字符正则  
	return str.replace(patt, function(char:any) {
		var H:any, L:any, code:any;
		if (char.length === 2) {
			H = char.charCodeAt(0); // 取出高位  
			L = char.charCodeAt(1); // 取出低位  
			code = (H - 0xD800) * 0x400 + 0x10000 + L - 0xDC00; // 转换算法  
			return "&#" + code + ";";
		} else {
			return char;
		}
	});
}
//将编码后的八进制的emoji表情重新解码成十六进制的表情字符
export function entitiesToUtf16(str:any) {
	return str.replace(/&#(\d+);/g, function(_match:any, dec:any) {
		let H = Math.floor((dec - 0x10000) / 0x400) + 0xD800;
		let L = Math.floor(dec - 0x10000) % 0x400 + 0xDC00;
		return String.fromCharCode(H, L);
	});
}
export function toArr(str:any,o=",") {
	return str.split(o)
}
export function arrToStr(arr:any,o=","){
	var str = ""
	for (var i = 0; i < arr.length; i++) {
		str = str + arr[i] + o
	}
	var reg = /,$/gi;
	str = str.replace(reg, "");
	return str;
}
export default {
	arrToStr,
	toArr,
	entitiesToUtf16,
	utf16toEntities,
	toZhongwen
}
