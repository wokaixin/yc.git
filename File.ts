	// blob图片转base64 
	/*
	* url='blob:http://localhost/fd.jpg'
	* callback=function 回调函数
	* outputFormat='image/png'
	*/
	function blobUrlToBase64(url:string,  outputFormat:string){
		return new Promise(function(resolve, reject) {
	var canvas:HTMLCanvasElement  = document.createElement("canvas"),
		ctx:CanvasRenderingContext2D|null = canvas.getContext('2d');
		if(ctx!=null){
			var img = new Image();
			img.crossOrigin = 'Anonymous';
			img.onload = function(){
			   canvas.height = img.height;
			   canvas.width = img.width;
			   if(ctx==null){
				reject("失败")
				return
			   }
			   ctx.drawImage(img,0,0);
			   var dataURL = canvas.toDataURL(outputFormat || 'image/jpeg');
			   canvas.parentNode?.parentNode?.removeChild(canvas)
			   resolve({dataURL,img})
			//    callback(dataURL,this);
			   
			};
			img.src = url;
		}
		reject("失败")
		})
	}
// obj.maxSize 最大字节数限制 ，超过压缩
// obj.width
// obj.height
// obj.width
// obj.height
//obj.name:'test.jpeg'
///obj.type:'image/jpeg'
// 压缩图片
 function photoCompress(file:any, obj:any) {
	return new Promise(function(resolve:any) {
  
	  const isLt = (file.size / 1024 / 1024) / obj.maxSize;
	  console.log(isLt)
	  if (isLt < 1) {
		resolve(file);
	  }
	  obj.quality = obj.quality ? obj.quality : 1 / isLt
	  console.log(obj.quality)
	  var ready = new FileReader();
	  /*开始读取指定的Blob对象或File对象中的内容. 当读取操作完成时,readyState属性的值会成为DONE,如果设置了onloadend事件处理程序,则调用之.同时,result属性中将包含一个data: URL格式的字符串以表示所读取文件的内容.*/
	  ready.readAsDataURL(file);
	  ready.onload = function() {
		var re = this.result;
		obj.type = obj.type ? obj.type : file.type
		obj.name = obj.name ? obj.name : file.name
		resolve(canvasDataURL(re, obj));
	  }
	});
  
  }
 function canvasDataURL(path:any, obj:any) {
	var img = new Image();
	img.src = path;
	return new Promise(function(resolve, reject) {
	  img.onload = function(that) {
		// 默认按比例压缩
		var w =img.width,
		  h =img.height,
		  scale = w / h;
		w = obj.width || w;
		h = obj.height || (w / scale);
		var quality = 0.3; // 默认图片质量为0.7
		//生成canvas
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
		// 创建属性节点
		var anw = document.createAttribute("width");
		anw.nodeValue = w+"";
		var anh = document.createAttribute("height");
		anh.nodeValue = h+"";
		canvas.setAttributeNode(anw);
		canvas.setAttributeNode(anh);
		if(ctx==null){
			reject("失败")
			return
		}
		ctx.drawImage(img, 0, 0, w, h);
		// 图像质量
		if (obj.quality && obj.quality <= 1 && obj.quality > 0) {
		  quality = obj.quality;
		}
		// quality值越小，所绘制出的图像越模糊
		var base64 = canvas.toDataURL('image/jpeg', quality);
  
		// base64再转file文件
		let arr = base64.split(',');

		let mimearr=	arr[0].match(/:(.*?);/)
		if(mimearr==null){
			reject("失败")
			return
		}
		let  mime = mimearr[1], // 转成blob
		  bstr = atob(arr[1]),
		  n = bstr.length,
		  u8arr = new Uint8Array(n);
		while (n--) {
		  u8arr[n] = bstr.charCodeAt(n);
		}
		let files = new window.File([new Blob([u8arr], {
		  type: mime
		})], obj.name ? obj.name : 'test.jpeg', {
		  type: obj.type ? obj.type : 'image/jpeg'
		}) // 转成file
		resolve(files)
	  }
	})
  
	// callback(files) // 回调
  }
  
export{
	blobUrlToBase64,
	canvasDataURL,
	photoCompress
}
export default {
	blobUrlToBase64,
	canvasDataURL,
	photoCompress
}